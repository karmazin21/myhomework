﻿using com;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Controller : MonoBehaviour
{
#region UI_Objects


    [SerializeField]
    private GameObject _Form;
    [SerializeField]
    private Toggle _Tgl;
    [SerializeField]
    private Button _BtnSave;
    [SerializeField]
    private Button _BtnCancel;
    [SerializeField]
    private InputField _Title;
    [SerializeField]
    private Dropdown _Type;
    [SerializeField]
    private InputField _Description;
    [SerializeField]
    private InputField _Effort;
    [SerializeField]
    private Image _Error;
    [SerializeField]
    private Image _Complete;
    [SerializeField]
    private Dropdown _Language;
    private int i = 0;
    #endregion

    #region Text
    [SerializeField]
    private Text _title;
    [SerializeField]
    private Text _type;
    [SerializeField]
    private Text _description;
    [SerializeField]
    private Text _effort;
    [SerializeField]
    private Text _termsAccepted;
    [SerializeField]
    private Text _save;
    [SerializeField]
    private Text _cancel;
    #endregion

#region Methods
    void Start()
    {
        AddListener();
        //ValueLanguage(0);
    }
    private void OnClickCancel()
    {
        _Title.text = "";
        _Type.value = 0;
        _Description.text = "";
        _Effort.text = "";
        _Tgl.isOn = false;
        
    }

    private void AddListener()
    {
         _Tgl.onValueChanged.AddListener(ValueToggleChange);
        _BtnCancel.onClick.AddListener(OnClickCancel);
        _BtnSave.onClick.AddListener(Save);
        _Language.onValueChanged.AddListener(ValueLanguage);
        EventManager.AddListener(EventManager.CHANGE_LANGUAGE, OnLanguageChange);
    }

    private void ValueToggleChange(bool _Tgl)
    {
        _BtnSave.gameObject.SetActive(_Tgl);
    }

    private void ValueLanguage(int index)
    {
        EventData eventData = new EventData(index);

        EventManager.DispatchEvent(EventManager.CHANGE_LANGUAGE, eventData);

        //EventManager.AddListener(EventManager.CHANGE_LANGUAGE, OnLanguageChange);
    }

    private void OnLanguageChange(EventData eventData)
    {
        if(_Language.value == 0)
        {
            _title.text = "Title";
            _type.text = "Type";
            _description.text = "Description";
            _effort.text = "Email";
            _termsAccepted.text = "Terms Accepted";
            _save.text = "Save";
            _cancel.text = "Cancel";
        } else if(_Language.value == 1)
        {
            _title.text = "Название";
            _type.text = "Тип";
            _description.text = "Описание";
            _effort.text = "Email";
            _termsAccepted.text = "Условия Использования";
            _save.text = "Сохранить";
            _cancel.text = "Отменить";
        }
        // здесь меняешь локаль конкретного текстового поля
    }

    private void Error()
    {
        if(_Title.text != "")
        {
            i++;
        }
        if(_Type.value != 0)
        {
            i++;
        }
        if(_Description.text != "")
        {
            i++;
        }
        if(_Effort.text != "")
        {
           i++;
        }
        if(_Tgl.isOn != false)
        {
            i++;
        }
    }
    private void Save()
    {
       Error();
       if(i < 5)
       {
           _Error.gameObject.SetActive(true);
           i = 0;
       } else if(i >= 5)
       {
           _Complete.gameObject.SetActive(true);
           i = 0;
           OnClickCancel();
       }
    }
}
#endregion