﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;



public class HeroUnit : MonoBehaviour
{
    [SerializeField]
    private Image _heroIcon;
    [SerializeField]
    private string _heroUrl;

    void Start()
    {
        StartCoroutine(LoadIcon(_heroUrl));
    }


    IEnumerator LoadIcon (string url)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);

        yield return request.SendWebRequest();
        //yield return new WaitForSeconds(10f);
        if (request.isNetworkError || request.isHttpError )
        {
            Debug.Log("Error");
        } else
        {
            Texture2D webTexture = ((DownloadHandlerTexture)request.downloadHandler).texture as Texture2D;
            Sprite webSprite = SpriteFromTexture(webTexture);
            _heroIcon.sprite = webSprite;

        }

        //yield return new WaitForSeconds(5f);
        //Log;
    }
    Sprite SpriteFromTexture(Texture2D texture)
    {
        return Sprite.Create(texture,new Rect(0.0f,0.0f, texture.width, texture.height), new Vector2(0,0), 100);
    }
}
