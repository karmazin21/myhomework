﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface MyInterface
{
    void AddReset();
    void AddAwake();
    void AddOnEnable();
    void AddStart();
    void AddFixedUpdate();
    void AddUpdate();

    void ShowList();

}

public class MyClass : MyInterface
{
    private List<string> myList = new List<string>();
    private bool _Fx = true;
    private bool _Update = true;
    public void AddReset()
    {
        myList.Add("Reset");
    }
    public void AddAwake()
    {
        myList.Add("Awake");
    }
    public void AddOnEnable()
    {
        myList.Add("OnEnable");
    }
    public void AddStart()
    {
        myList.Add("Start");
    }
    public void AddFixedUpdate()
    {
        if(_Fx == true)
        {
        myList.Add("FixedUpdate");
        _Fx = false;
        }
    }
    public void AddUpdate()
    {
        if(_Update == true)
        {
        myList.Add("Update");
        _Update = false;
        ShowList();
        }
    }

    public void ShowList()
    {
        foreach(object ob in myList)
        {
            Debug.Log(ob);
        }
    }
}
