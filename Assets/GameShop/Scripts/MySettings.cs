﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MySettings",menuName = "UnitSettings", order = 51)]
public class MySettings : ScriptableObject
{
    public string id;
    public int hp;
    public int attackRate;
    public int armoreRate;
    public int speed;
    public int price;
    public string url;
    public int unitCount;
}
