﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{
    [SerializeField]
    private Rigidbody2D Rb;
    [SerializeField]
    private float BallForce = 200;

    bool GameStarted = false;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.Space) && GameStarted == false)
        {
            transform.SetParent(null);// setParent
            Rb.isKinematic = false;
            Rb.AddForce(new Vector2(BallForce,BallForce));
            GameStarted = true;
        }
    }
}
