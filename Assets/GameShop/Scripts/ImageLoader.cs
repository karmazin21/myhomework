﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ImageLoader : MonoBehaviour
{
    [SerializeField]
    private Image icon;
    [SerializeField]
    private MySettings mySettings;

    void Start()
    {
        StartCoroutine(LoadIcon(mySettings.url));
    }
    IEnumerator LoadIcon (string url)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);

        yield return request.SendWebRequest();
        //yield return new WaitForSeconds(10f);
        if (request.isNetworkError || request.isHttpError )
        {
            Debug.Log("Error");
        } else
        {
            Texture2D webTexture = ((DownloadHandlerTexture)request.downloadHandler).texture as Texture2D;
            Sprite webSprite = SpriteFromTexture(webTexture);
            icon.sprite = webSprite;

        }

        //yield return new WaitForSeconds(5f);
        //Log;
    }
    Sprite SpriteFromTexture(Texture2D texture)
    {
        return Sprite.Create(texture,new Rect(0.0f,0.0f, texture.width, texture.height), new Vector2(0,0), 100);
    }
}
