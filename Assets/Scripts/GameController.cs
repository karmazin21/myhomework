﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private GameObject Platform;
    [SerializeField]
    private GameObject Ball;
    [SerializeField]
    private GameObject BricksObject;
    [SerializeField]
    private Text ScoreText;
    [SerializeField]
    private Image CompletePanel;
    [SerializeField]
    private Image StartPanel;
    [SerializeField]
    private Button BtnRestart;
    [SerializeField]
    private Button BtnPlay;
    private int Score = 0;
    private int Speed = 10;
    private int Brick;
    [SerializeField]
    private GameObject[] Bricks;
    void Start()
    {
        for(int i = 0; i < Bricks.Length; i++ )
         {
            Bricks[i] = GameObject.Find("Brick ("+i+")");// -1, Find, ()
            Brick++;
             
         }
         StartGamePanel();

         BtnRestart.onClick.AddListener(RestartLevel);
         BtnPlay.onClick.AddListener(PlayGame);
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            // check border
            Platform.transform.position -= new Vector3(Speed*Time.deltaTime,0,0);
        }
        if(Input.GetKey(KeyCode.RightArrow))
        {
            Platform.transform.position += new Vector3(Speed*Time.deltaTime,0,0);
        }
    }
    public void AddScore(int _score)
    {
        Score += _score;
        ScoreText.text = "Score: " + Score.ToString();
        Brick=Brick-1;
    }

    public void EndGame()
    {
        if(Brick <= 0)// num -> save to var
        {
            //Time.timeScale = 0.0000001f;
            CompletePanel.gameObject.SetActive(true);
            Platform.SetActive(false);
            Ball.SetActive(false);
        }
    }
    private void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void StartGamePanel()
    {
        Platform.SetActive(false);
        Ball.SetActive(false);
        BricksObject.SetActive(false);

        StartPanel.gameObject.SetActive(true);
    }
    private void PlayGame()
    {
        Platform.SetActive(true);
        Ball.SetActive(true);
        BricksObject.SetActive(true);

        StartPanel.gameObject.SetActive(false);
    }
}
