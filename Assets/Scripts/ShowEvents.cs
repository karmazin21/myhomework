﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowEvents : MonoBehaviour
{
    MyClass ob = new MyClass();
    
    void Reset()
    {
        ob.AddReset();
    }
    void Awake()
    {
        ob.AddAwake();
    }
    void OnEnable()
    {
        ob.AddOnEnable();
    }
    void Start()
    {
        ob.AddStart();
    }
    void FixedUpdate()
    {
        ob.AddFixedUpdate(); 
    }
    void Update()
    {
        ob.AddUpdate();
    }
}
