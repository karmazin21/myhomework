﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnitSpace;
using UnityEngine.UI;
using UnityEngine.Networking;
using com;
using System;


public class ShopController : MonoBehaviour
{
    const string PLAYER_SAVE = "_playerBalans";
    [SerializeField]
    private Text _playerBalansText;
    [SerializeField]
    private Transform _content;
    [SerializeField]
    private Transform _playerArmy;
    [SerializeField]
    private List<Unit> _purchasedItems = new List<Unit>();
    [SerializeField]
    private List<Unit> Units = new List<Unit>();
    [SerializeField]
    OrcUnit Orc;
    ReadJson Shop;

    //OrcUnit Orc = new OrcUnit("https://png.pngitem.com/pimgs/s/9-99768_warrior-clipart-orc-clipart-hd-png-download.png","Orc",5,5,5,5,10);
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.DeleteAll();
        
        AddOrc();
        EventManager.AddListener("buyUnit", AddBuyUnit);
        AddStartBalance();
        LoadData();

    }

    private void AddBuyUnit(EventData data)
    {
        Unit unit = (Unit)data.Data;
        if (Shop._playerBalans > unit.mySettings.price)
        {
            unit._purchased = true;
            _purchasedItems.Add(unit);
            unit.transform.SetParent(_playerArmy);
            Shop._playerBalans -= unit.mySettings.price;
            unit.buy.gameObject.SetActive(false);
            SaveData();
        }
        else
        {
            Debug.Log("Not enough money");
        }

        
    }

    /* public List<Unit> PurchasedItems
    {
        get
        {
            return _purchasedItems;
        }
        set
        {
            //_purchasedItems.Add(value);
        }
    }*/
    public void AddOrc()
    {
        Orc.CreateObj();
        for(int i = 0; i < Orc.mySettings.unitCount;i++)
        {
            var insOrc = Instantiate(Orc, _content);
            insOrc.CreateObj();
            Units.Add(insOrc);
        }
    }

    private void AddStartBalance()
    {
        if(!PlayerPrefs.HasKey(PLAYER_SAVE))
        {
            TextAsset ta = Resources.Load<TextAsset>("MyTxt");
            Shop = JsonUtility.FromJson<ReadJson>(ta.text);
            _playerBalansText.text = Shop._playerBalans.ToString();
        } else
        {
            //_playerBalansText.text = Shop._playerBalans.ToString();
            Shop._playerBalans = PlayerPrefs.GetInt(PLAYER_SAVE);
            _playerBalansText.text = Shop._playerBalans.ToString();
        }
        
    }

    private void SaveData()
    {
        PlayerPrefs.SetInt("_playerBalans",Shop._playerBalans);
        _playerBalansText.text = Shop._playerBalans.ToString();
        int i = 0;
        foreach(Unit unit in Units)
        {   
            PlayerPrefs.SetInt("Unit"+ i,Convert.ToInt32(unit._purchased));
            i++;
        }

    }
    private void LoadData()
    {

        /* for(int i = 0; i < Units.Count;i++)
        {
            Unit u = Units[i].GetComponent<Unit>();
            u._purchased = Convert.ToBoolean(PlayerPrefs.GetInt("Unit"+ i,0));
            if(u._purchased == true)
            {
                u.transform.SetParent(_playerArmy);
            }
        }*/
        int i = 0;
        foreach(Unit unit in Units)
        {   
            unit._purchased = Convert.ToBoolean(PlayerPrefs.GetInt("Unit"+ i,0));
            if(unit._purchased == true)
            {
                unit.transform.SetParent(_playerArmy);
                unit.buy.gameObject.SetActive(false);
            }
            i++;
        }

        
    }
    /*
    private void AddOrc()
    {
        Orc.Id = "Orc";
        Orc.Hp = 10;
        Orc.AttackRate = 5;
        Orc.ArmoreRate = 5;
        Orc.Speed = 4;
        Orc.Price = 50;
        Orc.CreateObj();
        for(int i = 0; i < 5;i++)
        {
            //OrcUnit Orc = _prefabOrc.GetComponent<OrcUnit>();
            Instantiate(Orc,_content);
            Units.Add(Orc);

        }
    }
    private void AddDragon()
    {
        
    }
    public void Buy(GameObject unit)
    {
        
        
        _purchasedItems.Add(s);
        //Instantiate(s,_playerArmy);
    }*/



}
public class ReadJson
{
    public int _playerBalans;
}
