﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickScript : MonoBehaviour
{
    private GameController GameController;
    [SerializeField]
    private int _countLives;
    [SerializeField]
    private int _countPoints;

    private int _countColision;

    void Start()
    {
        GameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnCollisionEnter2D(Collision2D Col)
    {
        if(Col.gameObject.tag == "Ball")
        {
            _countColision++;
            if(_countColision == _countLives)
            {
                GameController.AddScore(_countPoints);
                Destroy(gameObject);
                GameController.EndGame();
            }
            
            // EventManager
        }
    }
}
