﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com;

namespace UnitSpace
{
    class Char
    {
        public string id{get;set;}
        public int hp {get;set;}
        public int attackRate {get;set;}
        public int armoreRate {get;set;}
        public int speed {get;set;}
        public int price {get;set;}
    }
    public class Unit : MonoBehaviour
    {
        [SerializeField]
        private Text id;
        [SerializeField]
        private Text hp;
        [SerializeField]
        private Text attackRate;
        [SerializeField]
        private Text armoreRate;
        [SerializeField]
        private Text speed;
        [SerializeField]
        private Text price;
        public Button buy;
        public bool _purchased;
        //[SerializeField]
        //private ShopController shopController;
        /*
        private string _id;
        private int _hp;
        private int _attackRate;
        private int _armoreRate;
        private int _speed;
        private int _price;*/
        public MySettings mySettings;


        //public Char _char;


        /*
        public Unit(string id, int hp, int attackRate, int armoreRate, int speed, int price)
        {
            Id = id;
            Hp = hp;
            AttackRate = attackRate;
            ArmoreRate = armoreRate;
            Speed = speed;
            Price = price;
        }
         public Unit(Char @params) :base(@params.id,@params.hp, @params.speed,@params.attackRate,@params.armoreRate )
        {

        }

        public Char Char
        {
            get
            {
                return _char;
            }
            set
            {
                _char = value;
            }
        }
        
 
        public string Id  
        {
            get {return _id; }
            set {_id = value;}
        }
        public int Hp  
        {
            get {return _hp; }
            set {_hp = value;}
        }
        public int AttackRate  
        {
            get {return _attackRate; }
            set {_attackRate = value;}
        }
        public int ArmoreRate  
        {
            get {return _armoreRate; }
            set {_armoreRate = value;}
        }
        public int Speed  
        {
            get {return _speed; }
            set {_speed = value;}
        }
        public int Price  
        {
            get {return _price; }
            set {_price = value;}
        }

        */
        public virtual void CreateObj()
        {
            id.text = mySettings.id;
            hp.text = mySettings.hp.ToString();
            attackRate.text = mySettings.attackRate.ToString();
            armoreRate.text = mySettings.armoreRate.ToString();
            speed.text = mySettings.speed.ToString();
            price.text = mySettings.price.ToString();
            buy.onClick.AddListener(Buy);
        }

        public void Buy()
        {
            EventManager.DispatchEvent("buyUnit",new EventData(this));
        }
        

    }
}